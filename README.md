# Lab - Utilisation des Services Kubernetes pour Exposer des Pods

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

1. Exposer les pods du déploiement User-db en tant que service interne

2. Exposer les pods du déploiement de l'interface Web en tant que service externe


# Contexte 

Votre entreprise, Beebox, est en train de créer une application de microservice pour Kubernetes.

Ils disposent d'une base de données back-end contenant des informations sur les utilisateurs et d'une interface Web, toutes deux gérées à l'aide de déploiements. Ces deux applications sont également encore en cours de développement et utilisent actuellement uniquement de simples conteneurs Nginx pour les tests. Il vous a été demandé de configurer les services Kubernetes appropriés pour ces composants d'application.

La base de données utilisateur est un service backend qui ne doit être accessible que par les autres composants du cluster. L'interface Web doit être accessible aux utilisateurs extérieurs au cluster. Localisez les déploiements existants et créez les services nécessaires pour les exposer. Il existe un pod existant appelé busyboxque vous pouvez utiliser pour tester les services.

>![Alt text](img/image.png)


# Introduction
Les services Kubernetes constituent un excellent moyen de combiner la mise en réseau Kubernetes avec la nature dynamique et souvent automatisée des applications Kubernetes. Dans cet atelier, vous utiliserez les services pour exposer les pods Kubernetes existants. Cela vous permettra de mettre en pratique vos compétences avec les services Kubernetes.

# Application

## Étape 1 : Connexion au Serveur de Laboratoire

Connectez-vous au serveur à l'aide des informations d'identification fournies :

```sh
ssh -i id_rsa user@<PUBLIC_IP_ADDRESS>
```

## Étape 2 : Exposer les Pods du Déploiement `user-db` en Tant que Service Interne

1. Examinez les propriétés du déploiement `user-db` :

```sh
kubectl get deployment user-db -o yaml
```

2. Recherchez dans les propriétés de déploiement le **template** de pod et accordez une attention particulière aux labels, en particulier au label `app: user-db`. Notez quel(s) port(s) sont exposés.

>![Alt text](img/image-1.png)

3. Créez un fichier de service pour exposer ces pods à d'autres composants du cluster :

```sh
nano user-db-svc.yml
```

4. Définissez le service en utilisant :

```yaml
apiVersion: v1
kind: Service
metadata:
  name: user-db-svc
spec:
  type: ClusterIP
  selector:
    app: user-db
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```


*Exécution du services réussi*

5. Enregistrez et quittez le fichier


6. Créez le service :

```sh
kubectl create -f user-db-svc.yml
```

>![Alt text](img/image-3.png)
*Création du service ClusterIP*


7. Testez le service pour vous assurer qu'il fonctionne :

```sh
kubectl exec busybox -- curl user-db-svc
```

>![Alt text](img/image-4.png)
**

   - Cela pourrait ne pas fonctionner tout de suite. Vous devrez peut-être attendre quelques minutes et réessayer, mais vous devriez pouvoir voir la page d'accueil de Nginx.

## Étape 3 : Exposer les Pods du Déploiement `web-frontend` en Tant que Service Externe

1. Examinez les propriétés du déploiement `web-frontend` :

```sh
kubectl get deployment web-frontend -o yaml
```

>![Alt text](img/image-5.png)
*Détails du manifest de déploiement*

2. Vérifiez les labels appliquées au modèle de pod. Vous devriez voir le label `app=web-frontend`. Notez quel(s) port(s) sont exposés.

3. Créez un fichier de service pour exposer ces pods sur le port `30080` de chaque nœud de cluster :

```sh
nano web-frontend-svc.yml
```

4. Définissez le service en utilisant :

```yaml
apiVersion: v1
kind: Service
metadata:
  name: web-frontend-svc
spec:
  type: NodePort
  selector:
    app: web-frontend
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
      nodePort: 30080
```

5. Enregistrez et quittez le fichier

6. Créez le service :

```sh
kubectl create -f web-frontend-svc.yml
```

>![Alt text](img/image-6.png)
*Création du service NodePort*

7. Testez le service depuis l'extérieur du cluster (par exemple dans un navigateur) en accédant à `http://<PUBLIC_IP_ADDRESS>:30080`.

>![Alt text](img/image-7.png)
*Page d'acceuil de Nginx*

Ce laboratoire vous guide à travers le processus de configuration de services internes et externes pour vos déploiements Kubernetes, vous permettant d'exposer vos pods à d'autres composants du cluster et au monde extérieur.